package wy.movie.Po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Admin {
    private Long id;
    private Long idCard;
    private String username;
    private String password;
    private String realName;

}
