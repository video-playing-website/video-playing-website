package wy.movie.Po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Collect {
    private Long id;
    private User user;
    private Movie movie;
    private String title;
    private String content;
    private String type;
    private String picture;
    private Long movieId;
}
