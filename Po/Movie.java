package wy.movie.Po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Movie {
    private Long id;
    private String title;
    private String content;
    private String type;
    private String picture;
    private Date createTime;
    private Boolean hit;    //正在热播?
}
