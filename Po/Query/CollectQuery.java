package wy.movie.Po.Query;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CollectQuery {
    private Long userId;
    private Long movieId;
}
