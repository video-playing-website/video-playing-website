package wy.movie.Po.Query;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovieQuery {
    private Integer pageNum = 1;          //定义首页的页码
    private Integer pageSize =  16;       //每页显示多少个元素
    private String  type;

}
