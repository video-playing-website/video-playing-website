package wy.movie.Po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private Long id;
    private Long money;
    private String username;
    private String password;
    private String avatar;
    private Boolean vip;
    private Date vipStartTime;
    private Date vipStopTime;

}
