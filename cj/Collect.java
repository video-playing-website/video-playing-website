package wy.movie.Web;

import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import wy.movie.Po.Movie;
import wy.movie.Po.Query.CollectQuery;
import wy.movie.Po.Query.MovieQuery;
import wy.movie.Service.CollectService;
import wy.movie.Service.MovieService;
import wy.movie.Service.UserService;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

@Controller
public class Collect {

    @Autowired  //注入
    private CollectService collectService;

    @Autowired
    private UserService userService;

    @Autowired
    private MovieService movieService;

    @PostMapping("/addCollect")
    public  String addCollect(@RequestParam Long userId,
                              @RequestParam Long movieId,
                              wy.movie.Po.Collect collect,
                              RedirectAttributes attributes){

        Movie movie1 =  movieService.listMovieById(movieId);

        collect.setType(movie1.getType());
        collect.setPicture(movie1.getPicture());
        collect.setContent(movie1.getContent());
        collect.setTitle(movie1.getTitle());
        collect.setMovie(movieService.listMovieById(movieId));
        collect.setUser(userService.listUserById(userId));
        collect.setMovieId(movieId);

        collectService.addCollect(collect);
        attributes.addFlashAttribute("message","收藏成功！");

        return "redirect:/movie/play/"+movieId+"/"+userId;
    }

    @PostMapping("/deleteCollect")
    public  String deleteCollect(@RequestParam Long userId,
                                 @RequestParam Long movieId,
                                 wy.movie.Po.Collect collect,
                                 RedirectAttributes attributes){

        collectService.deleteCollect(collectService.checkCollect(userService.listUserById(userId),movieService.listMovieById(movieId)).getId());
        attributes.addFlashAttribute("message","取消收藏成功！");
        return "redirect:/movie/play/"+movieId+"/"+userId;
    }


    @GetMapping("/history/{userId}")
    public  String history(@PathVariable Long userId,
                           Model model,
                           MovieQuery movieQuery){
        PageInfo<wy.movie.Po.Collect> moviePageInfo = collectService.listCollectById(userId);
        model.addAttribute("history",moviePageInfo);
        System.out.println(moviePageInfo);
        return "person";
    }


}
