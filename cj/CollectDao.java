package wy.movie.Dao;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import wy.movie.Po.Collect;
import wy.movie.Po.Movie;

import java.util.List;

@Mapper
@Repository
public interface CollectDao {

    //添加收藏
    public Collect addCollect(Collect collect);

    //通过用户和视频查找
    public Collect checkCollect(Long userId,Long movieId);

    //删除收藏
    public void deleteCollect(Long id);

    //通过用户id查找
    public List<Collect> listCollectById(Long userId);

    //通过用户id查找
    public List<Movie> listCollectMovieByUserId(Long userId);
}
