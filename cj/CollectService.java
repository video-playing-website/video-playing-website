package wy.movie.Service;

import com.github.pagehelper.PageInfo;
import wy.movie.Po.Collect;
import wy.movie.Po.Movie;
import wy.movie.Po.Query.CollectQuery;
import wy.movie.Po.User;

import java.util.List;

public interface CollectService {

    //增加收藏
    public Collect addCollect(Collect collect);

    //查找收藏
    public Collect checkCollect(User user, Movie movie);

    //删除收藏
    public void  deleteCollect(Long id);

    //通过用户id查找
    public PageInfo<Collect> listCollectById(Long userId);

    //通过用户id查找
    public PageInfo<Movie> listCollectMovieByUserId(Long userId);
}
