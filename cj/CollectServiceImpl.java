package wy.movie.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wy.movie.Dao.CollectDao;
import wy.movie.Po.Collect;
import wy.movie.Po.Movie;
import wy.movie.Po.User;


@Service//告诉springboot这是service层
public class CollectServiceImpl implements CollectService {

    @Autowired
    private CollectDao collectDao;

    @Override
    public Collect addCollect(Collect collect) {
        return collectDao.addCollect(collect);
    }

    @Override
    public Collect checkCollect(User user, Movie movie) {
        return collectDao.checkCollect(user.getId(),movie.getId());
    }

    @Override
    public void deleteCollect(Long id) {
        collectDao.deleteCollect(id);
    }

    @Override
    public PageInfo<Collect> listCollectById(Long userId) {
        PageHelper.startPage(1,10);
        return new PageInfo<Collect>(collectDao.listCollectById(userId));
    }

    @Override
    public PageInfo<Movie> listCollectMovieByUserId(Long userId) {
        PageHelper.startPage(1,10);
        return new PageInfo<Movie>(collectDao.listCollectMovieByUserId(userId));
    }
}
