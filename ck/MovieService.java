package wy.movie.Service;

import com.github.pagehelper.PageInfo;
import wy.movie.Po.Movie;
import wy.movie.Po.Query.MovieQuery;
import wy.movie.Po.Query.UserQuery;
import wy.movie.Po.User;

import java.util.List;

public interface MovieService {

    //查询所有电影
    public List<Movie> listMovie();

    //根据类型查询电影                        //输入数据的类型
    public PageInfo<Movie> listMovieByType(MovieQuery movieQuery);



    //根据id查找电影
    public Movie listMovieById(Long id);

    //根据热播查找电影
    public PageInfo<Movie> listMovieByHit(Boolean hit);

    //通过电影名查找电影
    public PageInfo<Movie> listMovieByTitle(String title);

}
