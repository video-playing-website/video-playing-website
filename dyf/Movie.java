package wy.movie.Web;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import wy.movie.Po.Query.MovieQuery;
import wy.movie.Service.CollectService;
import wy.movie.Service.MovieService;
import wy.movie.Service.UserService;

@Controller
@RequestMapping("/movie")
public class Movie{

    @Autowired
    private MovieService movieService;

    @Autowired
    private CollectService collectService;

    @Autowired
    private UserService userService;

    //国产电影
    @GetMapping("/gc")
    public  String gc(Model model, MovieQuery movieQuery){
        movieQuery.setType("国产");
        PageInfo<wy.movie.Po.Movie> moviePageInfo = movieService.listMovieByType(movieQuery);
        model.addAttribute("page",moviePageInfo);
        System.out.println(moviePageInfo);
        return "movie_gc";
    }

    @GetMapping("/mg")
    public  String mg(Model model,MovieQuery movieQuery){
        movieQuery.setType("美国");
        PageInfo<wy.movie.Po.Movie> moviePageInfo = movieService.listMovieByType(movieQuery);
        model.addAttribute("page",moviePageInfo);
        return  "movie_mg";
    }

    @GetMapping("/dm")
    public  String dm(Model model, MovieQuery movieQuery){
        movieQuery.setType("动漫");
        PageInfo<wy.movie.Po.Movie> moviePageInfo = movieService.listMovieByType(movieQuery);
        model.addAttribute("page",moviePageInfo);
        return  "movie_dm";
    }


    @GetMapping("/wl")
    public  String wl(Model model, MovieQuery movieQuery){
        movieQuery.setType("网络");
        PageInfo<wy.movie.Po.Movie> moviePageInfo = movieService.listMovieByType(movieQuery);
        model.addAttribute("page",moviePageInfo);
        return  "movie_wl";
    }


    @GetMapping("/vip")
    public  String vip(Model model, MovieQuery movieQuery){

        movieQuery.setType("vip");
        PageInfo<wy.movie.Po.Movie> moviePageInfo = movieService.listMovieByType(movieQuery);
        model.addAttribute("page",moviePageInfo);
        return  "movie_vip";
    }

    //视频播放界面
    @GetMapping("/play/{movieId}/{userId}")
    public  String play(@PathVariable Long movieId,
                        @PathVariable Long userId,
                        Model model,//存放请求交互的值
                        RedirectAttributes attributes){
        if(userId == 0){
            attributes.addFlashAttribute("message","登录才能看电影哦~");
            return "redirect:/index";
        }else {
            if(userService.listUserById(userId).getVip() == false && movieService.listMovieById(movieId).getType().equals("vip")){
                attributes.addFlashAttribute("message","对不起，该影片是vip专享哦~");
                return "redirect:/movie/vip";
            }else {
                if( collectService.checkCollect(userService.listUserById(userId),movieService.listMovieById(movieId)) != null ){
                    model.addAttribute("collect",true);
                }else {
                    model.addAttribute("collect",false);
                }
                model.addAttribute("movie",movieService.listMovieById(movieId));
                return "play";
            }

        }

    }


    @GetMapping("/search")
    public String search(String query,Model model,RedirectAttributes attributes){
        PageInfo<wy.movie.Po.Movie> moviePageInfo = movieService.listMovieByTitle(query) ;
        model.addAttribute("movie",moviePageInfo);
        return "movie_search";
    }

}
