package wy.movie.Dao;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import wy.movie.Po.Movie;
import wy.movie.Po.Query.MovieQuery;

import java.util.List;

@Mapper
@Repository
public interface MovieDao {

    //查询所有电影
    public List<Movie> listMovie();

    //根据类型查找电影
    public List<Movie> listMovieByType(MovieQuery movieQuery);

    //根据id查找电影
    public Movie listMovieById(Long id);

    //根据热播查找电影
    public List<Movie> listMovieByHit(Boolean hit);

    //通过电影名查找电影
    public List<Movie> listMovieByTitle(String title);
}
