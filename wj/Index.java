package wy.movie.Web;


import com.github.pagehelper.PageInfo;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import wy.movie.Po.Movie;
import wy.movie.Po.Query.MovieQuery;
import wy.movie.Po.User;
import wy.movie.Service.MovieService;
import wy.movie.Service.UserService;
import wy.movie.Util.VerifyCode;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.server.PathParam;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;

@Controller
public class Index {

    @Autowired
    private MovieService movieService;

    @Autowired
    private UserService userService;

    @GetMapping("/")
    public String in(){
        return "redirect:/index";
    }

    @GetMapping("/index")
    public  String index(Model model,
                         MovieQuery movieQuery,
                         HttpSession session){

        if(log == false){
            session.setAttribute("id",0);  //id为0，表示没有登录
        }

        //利用正在热播查找电影
        model.addAttribute("hit",movieService.listMovieByHit(true));

        movieQuery.setType("国产");
        movieQuery.setPageSize(5);
        PageInfo<Movie> moviePageInfo = movieService.listMovieByType(movieQuery);
        model.addAttribute("gc",moviePageInfo);

        movieQuery.setType("美国");
        PageInfo<Movie> moviePageInfo2 = movieService.listMovieByType(movieQuery);
        model.addAttribute("mg",moviePageInfo2);

        movieQuery.setType("动漫");
        PageInfo<Movie> moviePageInfo3 = movieService.listMovieByType(movieQuery);
        model.addAttribute("dm",moviePageInfo3);

        movieQuery.setType("网络");
        PageInfo<Movie> moviePageInfo4 = movieService.listMovieByType(movieQuery);
        model.addAttribute("wl",moviePageInfo4);

        movieQuery.setType("vip");
        PageInfo<Movie> moviePageInfo5 = movieService.listMovieByType(movieQuery);
        model.addAttribute("vip",moviePageInfo5);

        return "index";
    }

    @GetMapping("/login")
    public  String login(){
        return "login";
    }

    public boolean log = false;

    //全局变量 验证码
    String Code = null;

    @PostMapping("/login")
    public  String login(@RequestParam String username,
                         @RequestParam String password,
                         @RequestParam String verifycode,
                         HttpSession session,
                         RedirectAttributes attributes){
        if(verifycode.equals(Code) ){   //验证码判断
        if(userService.checkUser(username,password) != null){
            log = true;
            User user = userService.checkUser(username,password);
            attributes.addFlashAttribute("message","欢迎登录"+","+username);
            session.setAttribute("id",user.getId());
            session.setAttribute("username",user.getUsername());
            session.setAttribute("avatar",user.getAvatar());
            if (user.getVip().equals(true)){
                session.setAttribute("vip",user.getVip());
            }
            return "redirect:/index";
        }
        else {
            attributes.addFlashAttribute("message","用户名或密码错误！");
            return "redirect:/login";
        }
        }
        else {
            attributes.addFlashAttribute("message","验证码输入错误！");
            return "redirect:/login";
        }

    }



    @RequestMapping("/getVerifyCode")
    public void getVerificationCode(HttpServletResponse response, HttpServletRequest request) {
        try {
            int width = 200;
            int height = 50;

            BufferedImage verifyImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);//生成对应宽高的初始图片
            String randomText = VerifyCode.drawRandomText(width, height, verifyImg);//单独的一个类方法，出于代码复用考虑，进行了封装。功能是生成验证码字符并加上噪点，干扰线，返回值为验证码字符

            request.getSession().setAttribute("verifyCode", randomText);
            response.setContentType("image/png");//必须设置响应内容类型为图片，否则前台不识别

            OutputStream os = response.getOutputStream(); //获取文件输出流
            ImageIO.write(verifyImg, "png", os);//输出图片流
            Code = randomText;
            os.flush();
            os.close();//关闭流
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @GetMapping("/loginOut")
    public  String loginOut(HttpSession session,RedirectAttributes attributes){
        log = false;
        session.removeAttribute("id");
        session.removeAttribute("username");
        session.removeAttribute("avatar");
        session.removeAttribute("vip");
        attributes.addFlashAttribute("message","注销成功！");
        return "redirect:/index";
    }

}
