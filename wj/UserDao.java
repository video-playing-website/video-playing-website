package wy.movie.Dao;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import wy.movie.Po.Query.UserQuery;
import wy.movie.Po.User;

import java.util.List;

@Mapper
@Repository
public interface UserDao {

    //查询所有用户
    public List<User> listUser();

    //根据用户名查询用户
    public List<User> listUserByUsername(UserQuery userQuery);

    //根据用户名密码查询用户
    public User checkUser(String username,String password);

    //根据id查询用户
    public  User listUserById(Long id);
}
