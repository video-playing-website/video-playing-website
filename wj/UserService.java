package wy.movie.Service;

import com.github.pagehelper.PageInfo;
import wy.movie.Po.Query.UserQuery;
import wy.movie.Po.User;

import java.util.List;

public interface UserService {

    //查询所有用户
    public List<User> listUser();

    //根据用户名查询用户
    public PageInfo<User> listUserByUsername(UserQuery userQuery);

    //根据用户名密码查询用户
    public  User checkUser(String username,String password);

    //根据ID查询用户
    public User listUserById(Long id);
}
