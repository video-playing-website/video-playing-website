package wy.movie.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wy.movie.Dao.UserDao;
import wy.movie.Po.Query.UserQuery;
import wy.movie.Po.User;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public List<User> listUser() {
        return userDao.listUser();
    }

    @Override
    public PageInfo<User> listUserByUsername(UserQuery userQuery) {
        PageHelper.startPage(userQuery.getPageNum(),userQuery.getPageSize());
        return new  PageInfo<User>(userDao.listUserByUsername(userQuery));
    }

    @Override
    public User checkUser(String username, String password) {
        return userDao.checkUser(username,password);
    }

    @Override
    public User listUserById(Long id) {
        return userDao.listUserById(id);
    }
}
