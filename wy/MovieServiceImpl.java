package wy.movie.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wy.movie.Dao.MovieDao;
import wy.movie.Po.Movie;
import wy.movie.Po.Query.MovieQuery;
import wy.movie.Po.User;

import java.util.List;

@Service
public class MovieServiceImpl implements MovieService {

    @Autowired
    private MovieDao movieDao;

    @Override
    public List<Movie> listMovie() {
        return movieDao.listMovie();
    }

    @Override
    public PageInfo<Movie> listMovieByType(MovieQuery movieQuery) {
        PageHelper.startPage(movieQuery.getPageNum(),movieQuery.getPageSize());
        return new PageInfo<Movie>(movieDao.listMovieByType(movieQuery));
    }

    @Override
    public Movie listMovieById(Long id) {
        return movieDao.listMovieById(id);
    }

    @Override
    public PageInfo<Movie> listMovieByHit(Boolean hit) {
        PageHelper.startPage(1,5);
        return new PageInfo<Movie>(movieDao.listMovieByHit(hit));
    }

    @Override
    public PageInfo<Movie> listMovieByTitle(String title) {
        PageHelper.startPage(1,50);
        return new PageInfo<Movie>(movieDao.listMovieByTitle(title));
    }
}
